package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestCtrl {

	@RequestMapping(path="/hello", method = RequestMethod.GET)
	public String sayHello() {
		return "Hello from Harish.";
	}
}
